package com.example.r3mii62.td3;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MyVideosGamesAdapter extends RecyclerView.Adapter<MyVideosGamesAdapter.MyViewHolder>
{

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView mNameTV;
        private TextView mPriceTV;
        private ImageView mImgTV;

        MyViewHolder(View itemView)
        {
            super(itemView);
            mNameTV = itemView.findViewById(R.id.Game_name);
            mPriceTV = itemView.findViewById(R.id.Game_price);
            mImgTV = itemView.findViewById(R.id.Img_Game);
        }

        void display(JeuVideo jeuVideo)
        {
            mNameTV.setText(jeuVideo.getName());
            mPriceTV.setText(jeuVideo.getPrice() + "€");
            mImgTV.setImageResource(jeuVideo.getImg());
        }
    }

    List<JeuVideo> mesJeux;
    MyVideosGamesAdapter(List<JeuVideo> mesJeux)
    {
        this.mesJeux = mesJeux;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.jeu_video_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        holder.display(mesJeux.get(position));
    }

    @Override
    public int getItemCount()
    {
        return mesJeux.size();
    }
}
