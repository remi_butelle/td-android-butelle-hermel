package com.example.r3mii62.td3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private List<JeuVideo> mesJeux;
    private MyVideosGamesAdapter monAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.R_View);

        mesJeux = new ArrayList<>();

        mesJeux.add(new JeuVideo("Forza Horizon 3", 49.90F,R.drawable.forza_horizon3));
        mesJeux.add(new JeuVideo("Forza Motorsport 5", 20.00F,R.drawable.forza_motorsport5));
        mesJeux.add(new JeuVideo("Asseto Corsa", 22.00F,R.drawable.asseto_corsa));
        mesJeux.add(new JeuVideo("Gran Theft Auto 5", 49.90F,R.drawable.gta5));
        mesJeux.add(new JeuVideo("Pokemon Let's Go : Pikachu", 60.90F,R.drawable.lets_go_pikachu));
        mesJeux.add(new JeuVideo("Pokemon Let's Go : Evoli", 60.90F,R.drawable.lets_go_evoli));
        mesJeux.add(new JeuVideo("Mario Party", 49.90F,R.drawable.mario_party));
        mesJeux.add(new JeuVideo("Animal Crossing", 30.00F,R.drawable.animal_crossing));
        mesJeux.add(new JeuVideo("Spider-Man", 69.90F,R.drawable.spiderman));
        mesJeux.add(new JeuVideo("Monster Hunter : World", 49.90F,R.drawable.monster_hunter_world));

        monAdapter = new MyVideosGamesAdapter(mesJeux);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        //mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        //mRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),3));
        mRecyclerView.setAdapter(monAdapter);
    }
}
