package com.example.r3mii62.td3;


public class JeuVideo {
    private String name;
    private float price;
    private int img;

    JeuVideo(String name, float price, int img) {
        this.name = name;
        this.price = price;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getImg() {
        return img;
    }
}
