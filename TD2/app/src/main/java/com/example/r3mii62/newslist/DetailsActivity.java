package com.example.r3mii62.newslist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "NewsList";
    Button b_ok;
    TextView t_login_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setTitle(getLocalClassName());
        Log.i(TAG, "Début de l'activité " + getLocalClassName());
        b_ok = findViewById(R.id.B_ok);
        b_ok.setOnClickListener(this);
        t_login_details = findViewById(R.id.T_login_details);
        NewsListApplication app = (NewsListApplication) getApplicationContext();
        t_login_details.setText(app.getLogin());
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId())
        {
            case R.id.B_ok:
                intent = new Intent(this,NewsActivity.class);
                Log.i(TAG,"Tu as cliqué sur le bouton " + b_ok.getText());
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "terminaison de l'activité " + getLocalClassName());
    }
}
