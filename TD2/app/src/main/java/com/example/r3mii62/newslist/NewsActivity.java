package com.example.r3mii62.newslist;

import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NewsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "NewsList";
    Button b_details;
    Button b_logout;
    Button b_libelle;
    String login;
    TextView t_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        setTitle(getLocalClassName());
        Log.i(TAG, "Début de l'activité " + getLocalClassName());
        b_details = findViewById(R.id.B_details);
        b_logout = findViewById(R.id.B_logout);
        b_libelle = findViewById(R.id.B_libelle);
        b_details.setOnClickListener(this);
        b_logout.setOnClickListener(this);
        b_libelle.setOnClickListener(this);
        Intent intent = getIntent();
        login = intent.getStringExtra("login");
        t_login = findViewById(R.id.T_login_news);
        t_login.setText(login);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId())
        {
            case R.id.B_details:
                intent = new Intent(this,DetailsActivity.class);
                Log.i(TAG,"Tu as cliqué sur le bouton " + b_details.getText());
                startActivity(intent);
                finish();
                break;
            case R.id.B_logout:
                intent = new Intent(this,LoginActivity.class);
                Log.i(TAG,"Tu as cliqué sur le bouton " + b_logout.getText());
                startActivity(intent);
                finish();
                break;
            case R.id.B_libelle:
                Log.i(TAG,"Tu as cliqué sur le bouton " + b_libelle.getText());
                String url = "http://android.busin.fr/";
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "terminaison de l'activité " + getLocalClassName());
    }
}
