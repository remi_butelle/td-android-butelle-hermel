package com.example.r3mii62.newslist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "NewsList";
    EditText e_login;
    Button b_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        setTitle(getLocalClassName());
        Log.i(TAG, "Début de l'activité " + getLocalClassName());
        b_login = findViewById(R.id.B_login);
        b_login.setOnClickListener(this);
        e_login = findViewById(R.id.E_login);

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        NewsListApplication app = (NewsListApplication) getApplicationContext();
        switch (v.getId())
        {
            case R.id.B_login:
                intent = new Intent(this,NewsActivity.class);
                intent.putExtra("login", e_login.getText().toString());
                app.setLogin(e_login.getText().toString());
                Log.i(TAG,"Tu as cliqué sur le bouton " + b_login.getText());
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i(TAG, "terminaison de l'activité " + getLocalClassName());
    }
}
