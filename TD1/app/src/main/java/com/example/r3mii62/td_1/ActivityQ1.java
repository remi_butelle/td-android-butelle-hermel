package com.example.r3mii62.td_1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityQ1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        number1 = findViewById(R.id.number1);
        number2 = findViewById(R.id.number2);
        ResAdd = findViewById(R.id.ViewResAdd);
        ResSub = findViewById(R.id.ViewResSub);
        ResMul = findViewById(R.id.ViewResMul);
        ResDiv = findViewById(R.id.ViewResDiv);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Nice try :)", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final Button b_result = findViewById(R.id.buttonResult);
        b_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                computeAdd(v);
            }
        });

        final Button b_Main = findViewById(R.id.goToMain);
        b_Main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ1.this,MainActivity.class);
                startActivity(intent);
            }
        });

        final Button b_Q2 = findViewById(R.id.goToQ2);
        b_Q2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ1.this,ActivityQ2.class);
                startActivity(intent);
            }
        });

        final Button b_Q3 = findViewById(R.id.goToQ3);
        b_Q3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ1.this,ActivityQ3.class);
                startActivity(intent);
            }
        });

        final Button b_Q4 = findViewById(R.id.goToQ4);
        b_Q4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ1.this,ActivityQ4.class);
                startActivity(intent);
            }
        });
    }

    private EditText number1;
    private EditText number2;
    private TextView ResAdd;
    private TextView ResSub;
    private TextView ResMul;
    private TextView ResDiv;
    private Boolean error = true;
    private int intResAdd;
    private int intResSub;
    private int intResMul;
    private int intResDiv;


    public void computeAdd(View v){
        String valNum1 = number1.getText().toString();
        String valNum2 = number2.getText().toString();
        if(!TextUtils.isEmpty(valNum1) && !TextUtils.isEmpty(valNum2))
        {
            try {
                intResAdd = Integer.parseInt(valNum1) + Integer.parseInt(valNum2);
                if(Integer.parseInt(valNum1) > Integer.parseInt(valNum2))
                {
                    intResSub = Integer.parseInt(valNum1) - Integer.parseInt(valNum2);
                }
                else
                {
                    intResSub = Integer.parseInt(valNum2) - Integer.parseInt(valNum1);
                }
                intResMul = Integer.parseInt(valNum1) * Integer.parseInt(valNum2);
                if(Integer.parseInt(valNum2) == 0)
                {
                    intResDiv = 0;
                }
                else
                {
                    intResDiv = Integer.parseInt(valNum1) / Integer.parseInt(valNum2);
                }
                error = false;
            }
            catch (NumberFormatException e)
            {
                Log.w("xXx","erreur de conversion",e);
            }
        }
        if(!error)
        {
            ResAdd.setText(Integer.toString(intResAdd));
            ResSub.setText(Integer.toString(intResSub));
            ResMul.setText(Integer.toString(intResMul));
            ResDiv.setText(Integer.toString(intResDiv));
        }
    }

}
