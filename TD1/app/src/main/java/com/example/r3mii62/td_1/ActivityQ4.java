package com.example.r3mii62.td_1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityQ4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q4);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        affRes = findViewById(R.id.editCalc);
        final Button b_zero, b_un, b_deux, b_trois, b_quatre, b_cinq, b_six, b_sept, b_huit, b_neuf, b_reset, b_add, b_sub, b_mul, b_div, b_point, b_equal, b_ouvPar, b_ferPar, b_negNumber;

        b_zero = findViewById(R.id.b_zero);
        b_un = findViewById(R.id.b_un);
        b_deux = findViewById(R.id.b_deux);
        b_trois = findViewById(R.id.b_trois);
        b_quatre = findViewById(R.id.b_quatre);
        b_cinq = findViewById(R.id.b_cing);
        b_six = findViewById(R.id.b_six);
        b_sept = findViewById(R.id.b_sept);
        b_huit = findViewById(R.id.b_huit);
        b_neuf = findViewById(R.id.b_neuf);
        b_reset = findViewById(R.id.buttonReset);
        b_point = findViewById(R.id.b_point);
        b_ouvPar = findViewById(R.id.parentheseOuvert);
        b_ferPar = findViewById(R.id.parentheseFerme);
        b_negNumber = findViewById(R.id.buttonNeg);
        b_add = findViewById(R.id.buttonAdd);
        b_div = findViewById(R.id.buttonDiv);
        b_mul = findViewById(R.id.buttonMul);
        b_sub = findViewById(R.id.buttonSub);
        b_equal = findViewById(R.id.buttonResult);

        b_zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "0");
            }
        });

        b_un.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "1");
            }
        });

        b_deux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "2");
            }
        });

        b_trois.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "3");
            }
        });

        b_quatre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "4");
            }
        });

        b_cinq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "5");
            }
        });

        b_six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "6");
            }
        });

        b_sept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "7");
            }
        });

        b_huit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "8");
            }
        });

        b_neuf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "9");
            }
        });

        b_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(null);
            }
        });

        b_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + ".");
            }
        });

        b_equal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        b_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "+");
            }
        });

        b_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "-");
            }
        });

        b_mul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "*");
            }
        });

        b_div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "/");
            }
        });

        b_ouvPar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + "(");
            }
        });

        b_ferPar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                affRes.setText(affRes.getText() + ")");
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Nice try :)", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final Button b_Main = findViewById(R.id.goToMain);
        b_Main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ4.this,MainActivity.class);
                startActivity(intent);
            }
        });

        final Button b_Q1 = findViewById(R.id.goToQ1);
        b_Q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ4.this,ActivityQ1.class);
                startActivity(intent);
            }
        });

        final Button b_Q2 = findViewById(R.id.goToQ2);
        b_Q2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ4.this,ActivityQ2.class);
                startActivity(intent);
            }
        });

        final Button b_Q3 = findViewById(R.id.goToQ3);
        b_Q3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ4.this,ActivityQ3.class);
                startActivity(intent);
            }
        });
    }

    private EditText affRes;

}
