package com.example.r3mii62.td_1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.nio.BufferUnderflowException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        number1 = findViewById(R.id.number1);
        number2 = findViewById(R.id.number2);
        aff_resultat = findViewById(R.id.textView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Nice try :)", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final Button b_result = findViewById(R.id.buttonResult);
        b_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                computeAdd(v);
            }
        });

        final Button b_Q1 = findViewById(R.id.goToQ1);
        b_Q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ActivityQ1.class);
                startActivity(intent);
            }
        });

        final Button b_Q2 = findViewById(R.id.goToQ2);
        b_Q2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActivityQ2.class);
                startActivity(intent);
            }
        });

        final Button b_Q3 = findViewById(R.id.goToQ3);
        b_Q3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActivityQ3.class);
                startActivity(intent);
            }
        });

        final Button b_Q4 = findViewById(R.id.goToQ4);
        b_Q4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActivityQ4.class);
                startActivity(intent);
            }
        });

    }


    private EditText number1;
    private EditText number2;
    private int res2;
    private TextView aff_resultat;
    private Boolean error = true;


    public void computeAdd(View v){
        String valNum1 = number1.getText().toString();
        String valNum2 = number2.getText().toString();
        if(!TextUtils.isEmpty(valNum1) && !TextUtils.isEmpty(valNum2))
        {
            try {
                res2 = Integer.parseInt(valNum1) + Integer.parseInt(valNum2);
                error = false;
            }
            catch (NumberFormatException e)
            {
                Log.w("xXx","erreur de conversion",e);
            }
        }
        if(!error)
        {
            aff_resultat.setText(Integer.toString(res2));
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
