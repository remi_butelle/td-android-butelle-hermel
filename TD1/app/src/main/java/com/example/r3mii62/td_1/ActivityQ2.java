package com.example.r3mii62.td_1;

import android.content.Intent;
import android.media.audiofx.DynamicsProcessing;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityQ2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editTextView = findViewById(R.id.editTextView);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Nice try :)", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final Button b_Add = findViewById(R.id.buttonAdd);
        b_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Addition();
            }
        });

        final Button b_result = findViewById(R.id.buttonResult);
        b_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Equal();
            }
        });

        final Button b_Main = findViewById(R.id.goToMain);
        b_Main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ2.this,MainActivity.class);
                startActivity(intent);
            }
        });

        final Button b_Q1 = findViewById(R.id.goToQ1);
        b_Q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ2.this,ActivityQ1.class);
                startActivity(intent);
            }
        });

        final Button b_Q3 = findViewById(R.id.goToQ3);
        b_Q3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ2.this,ActivityQ3.class);
                startActivity(intent);
            }
        });

        final Button b_Q4 = findViewById(R.id.goToQ4);
        b_Q4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityQ2.this,ActivityQ4.class);
                startActivity(intent);
            }
        });

    }


    private EditText editTextView;
    private int nbr1, nbr2;

    public void Addition()
    {
        try {
            if (editTextView == null) {
                editTextView.setText("");
            } else {
                Equal();
                nbr1 = Integer.parseInt(editTextView.getText() + "");
                editTextView.setText(null);
            }
        }
        catch (NumberFormatException e)
        {
            Log.w("xXx","erreur de conversion",e);
        }
    }

    public void Equal()
    {
        try{
            nbr2 = Integer.parseInt(editTextView.getText() + "");
            editTextView.setText(nbr1 + nbr2 + "");
        }
        catch (NumberFormatException e)
        {
            Log.w("xXx","erreur de conversion",e);
        }

    }

}
